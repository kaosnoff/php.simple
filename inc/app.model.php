<?php
namespace App\Model;

class AppModel
{
	private $config = [];
	protected $pdo;
	protected $lastError = null;
	public $table;
	public $primary_key = 'id';
	protected $alias;
	protected $fields = [];
	
	private $Router;
	
	protected $order = "";
	
	function __construct()
	{
		$this->Router = \Core\Router::getInstance();
		
		$this->alias = str_replace(__NAMESPACE__."\\",'',get_class($this));
		if (!$this->table)
		{
			$this->table = DeCamelCase($this->alias);
		}
		
		$this->connect();
		
		$this->pdo->exec("USE {$this->table};");
		
		if (!$this->order)
		{
			$this->order = "{$this->alias}.{$this->primary_key} ASC";
		}
	}
	
	private function getStructure()
	{
		if (!$this->fields or sizeof($this->fields) == 0)
		{
			$sql = "DESCRIBE {$this->table}";
			
			$fields = [];
			
			$res = $this->pdo->query($sql, \PDO::FETCH_ASSOC);
			
			if (!$res)
			{
				throw new \Exception("Tabela inexistente!");
			}
			
			foreach($res as $k=>$v)
			{
				$fields[$v['Field']] = $v;
			}
			
			$this->fields = $fields;
		}
	}
	
	protected function connect()
	{
		$config = $this->loadConfig();
		$options = array(
			\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
		); 
		try
		{
			$this->pdo = new \PDO("mysql:dbname={$config['DB_NAME']};host={$config['DB_HOST']}",$config['DB_USER'],$config['DB_PASSWORD'],$options);
		}
		catch(\PDOException $e)
		{
			throw new \Exception(sprintf(__("Erro ao conectar no Banco de Dado \"<strong>%s</strong>\"!"), $config['DB_NAME']),500);
		}
		
		$this->getStructure();
		
		return $this->pdo;
	}
	
	private function loadConfig()
	{
		$config = [];
		
		$path = str_replace('\\','/',ABSPATH)."";
		
		$configFile = $path."src/config/config.database.php";
		
		if (!file_exists($configFile))
		{
			throw new \Exception(sprintf(__("O arquivo de configuração da base de dados não foi encontrado em \"<strong>%s</strong>\"!"), $configFile),500);
		}
		
		include $configFile;
		
		return $config;
	}
	
	public function query($sql)
	{
		$saida = [];
		foreach($this->pdo->query($sql, \PDO::FETCH_ASSOC) as $k=>$v)
		{
			$saida[$k] = $v;
		}
		return $saida;
	}
	
	public function find($tipo='all',$conds = [],$order = "")
	{
		$fields = [];
		foreach($this->fields as $k=>$v)
		{
			$fields[$k] = $this->alias.'.'.$k;
		}
		$sql = "SELECT";
		$sql .= " ".implode(", ",$fields)." ";
		$sql .= " FROM {$this->table} {$this->alias}";
		$sql .= " WHERE 1=1";
		
		if ($conds)
		{
			foreach($conds as $k=>$v)
			{
				$sql .= " AND {$k} = ?";
			}
		}
		
		$sql .= " ORDER BY " . ($order ? $order : $this->order);
		
		$sth = $this->pdo->prepare($sql);
		
		$isOk = $sth->execute($conds ? array_values($conds): []);
		//var_dump($sth->queryString);
		if (!$isOk)
		{
			$erro = $sth->errorInfo();
			$this->Router->error = true;
			
			throw new \PDOException(sprintf(__("Erro no Banco de Dados ao executar o comando: \n\n<strong>%s</strong>\n\nErro: (%s): %s"), $sth->queryString, $erro[0], $erro[2]),500);
		}
		
		if ($tipo == 'first')
		{
			$res = $sth->fetch(\PDO::FETCH_ASSOC);
		}
		else
		{
			$res = $sth->fetchAll(\PDO::FETCH_ASSOC);
		}
		
		return $res;
	}
	
	public function add($item)
	{
		$item = $this->beforeSave($item);
		
		$sql = "INSERT INTO {$this->table}";
		
		if (!@$item[$this->alias][$this->primary_key])
		{
			$item[$this->alias][$this->primary_key] = null;
		}
		
		foreach($item as $model=>$data)
		{
			$sqlFields = [];
			$valores = [];
			foreach($data as $k=>$v)
			{
				$chave = ":{$k}";
				array_push($sqlFields,"{$k}");
				if (is_array($v)) $v = json_encode($v);
				$valores[$chave] = $v;
			}
		}
		$sql .= " (".implode(', ',$sqlFields).") VALUES (".implode(', ',array_keys($valores)).")";
		
		$sth = $this->pdo->prepare($sql);
		
		$res = $sth->execute($valores);
		
		if ($res === NULL) $res = true;
		
		if (!$res)
		{
			$this->lastError = $sth->errorInfo();
		}
		else
		{
			$lastId = $this->pdo->lastInsertId();
			if (!$lastId) $lastId = @$item[$this->alias][$this->primary_key];
		}
		return $res ? $lastId : false;
	}
	
	public function edit($item)
	{
		$item = $this->beforeSave($item);
		
		$sql = "UPDATE {$this->table}";
		
		$id = @$item[$this->alias][$this->primary_key];
		
		unset($item[$this->alias][$this->primary_key]);
		
		foreach($item as $model=>$data)
		{
			$sqlFields = [];
			$valores = [];
			foreach($data as $k=>$v)
			{
				$chave = ":{$k}";
				array_push($sqlFields,"{$k} = {$chave}");
				if (is_array($v)) $v = json_encode($v);
				$valores[$chave] = $v;
			}
			$valores[":{$this->primary_key}"] = $id;
		}
		$sql .= " SET ".implode(', ',$sqlFields);
		$sql .= " WHERE {$this->primary_key} = :{$this->primary_key}";
		
		$sth = $this->pdo->prepare($sql);
		$res = $sth->execute($valores);
		
		if (!$res)
		{
			$this->lastError = $sth->errorInfo();
		}
		
		return $res ? $id : false;
	}
	
	public function addForce($item)
	{
		$id = $this->add($item);
		return $id ? $id : $this->edit($item);
	}
	
	public function delete($id)
	{
		$sql = "DELETE FROM {$this->table} WHERE {$this->primary_key} = ?";
		
		$sth = $this->pdo->prepare($sql);
		$res = $sth->execute([$id]);
		
		if (!$res)
		{
			$this->lastError = $sth->errorInfo();
		}
		
		return $res ? $id : false;
	}
	
	public function getError()
	{
		$last = $this->lastError;
		$this->lastError = "";
		
		return $last;
	}
	
	public function beforeSave($item)
	{
		if (!array_key_exists($this->alias,$item)) return $item;
		
		if (array_key_exists('created',$this->fields) and !array_key_exists('created',$item[$this->alias]) and @!$item[$this->alias][$this->primary_key])
		{
			$item[$this->alias]['created'] = date('c');//date('Y-m-d H:i:s');
		}
		
		return $item;
	}
	
	function __destruct()
	{
		//$this->pdo = null;
	}
}