<?php
namespace Core;

\Core\App::importModule('App.Controller');

class Router
{
	private $Core;
	private $path;
	private $method;
	
	public $controller 	= 'main';
	public $action 		= 'index';
	public $args 		= [];
	public $vars 		= [];
	
	public $data 		= [];
	
	public $error = false;
	
	private $rendered = false;
	
	function __construct()
	{
		$this->method = $_SERVER['REQUEST_METHOD'];
		
		$this->setPath();
		
		$this->getRequests();
	}
	
	# Pega a instância Singleton da classe
	public static function getInstance()
	{
		static $instance = null;
		if (null === $instance)
		{
			$instance = new static();
		}
		
		return $instance;
	}
	
	private function setPath($strPath = "")
	{
		if ($strPath)
		{
			$path = $strPath;
		}
		else
		{
			$path = '/'.@$_GET['path'];
		}
		
		$newpath = $this->processaPath($path);
		$this->path = $newpath;
		
		$this->controller = $newpath['controller'];
		$this->action = $newpath['action'];
		$this->args = $newpath['args'];
		
		$mypath = "/{$this->controller}/{$this->action}/".implode('/',$this->args);
		//$_SESSION['path'] = $mypath;
	}
	
	private function forcePath($strPath = "")
	{
		$_SESSION['path'] = $strPath;
		session_commit();
		
		$this->setPath($strPath);
	}
	
	# Descobre se a requisição foi de um tipo (método) específico
	public function is($method)
	{
		return strtoupper($method) == strtoupper($this->method);
	}
	
	# Redireciona o fluxo de informações para outro endereço
	public function redirect($strPath)
	{
		$url = self::getUrl($strPath);
		
		header("Location: {$url}");
		exit;
	}
	
	public static function getUrl($strPath,$full=false)
	{
		$path = BASEURL.ltrim($strPath,'/');
		
		if ($full)
		{
			$protocol = (@$_SERVER['HTTPS'] === "on") ? 'https' : 'http';
			$path = "{$protocol}://".$_SERVER['HTTP_HOST'].$path;
		}
		return $path;
	}
	
	# Força o valor da variável $rendered
	public function setRendered($value = true)
	{
		$this->rendered = $value;
	}
	
	# Renderiza uma ação de um controlador, de acordo com o endereço passado.
	public function render($strPath=null,$force=false)
	{
		if ($this->rendered and !$force) return;
		
		if ($this->error) return;
		
		if ($strPath)
		{
			$this->forcePath($strPath);
		}
		
		if (!\Core\App::isInstalled())
		{
			$base = str_replace('\\','/',ABSPATH.FRAMEWORK).'skeleton/controllers/';
			$file = $base."core".".controller.php";
		}
		else
		{
			$base = str_replace('\\','/',ABSPATH).'src/controllers/';
			$file = $base.($this->controller).".controller.php";
		}
		
		$cName = $this->getControllerName();
		
		if (!file_exists($file))
		{
			$this->error = true;
			throw new \Exception(sprintf(__("O Controlador \"<strong>%s</strong>\" não foi encontrado. Certifique-se de que o arquivo existe em \"<strong>%s</strong>\"!"), $cName,$file),404);
		}
		require_once $file;
		
		if (!class_exists('\\App\\Controller\\'.$cName, false))
		{
			$this->error = true;
			throw new \Exception(sprintf(__("O arquivo \"<strong>%s</strong>\" existe, mas não foi encontrada a classe \"<strong>%s</strong>\" correspondente!"), $file, $cName),404);
		}
		
		$class = '\\App\\Controller\\'.$cName;
		
		$c = new $class();
		
		if (!method_exists($c, $this->action))
		{
			$this->error = true;
			throw new \Exception(sprintf(__("O método \"<strong>%s</strong>\" não existe na classe \"<strong>%s</strong>\"!"), $this->action, $cName),404);
		}
		
		$action = $this->action;
		
		$c->beforeFilter();
		
		call_user_func_array([$c,$this->action], ($this->vars ? $this->vars : $this->args));
		
		$c->render();
		$this->rendered = true;
		$c->afterFilter();
	}
	
	# Retorna o resultado bruto de uma Ação dentro de um Controlador, sem forçar sua renderização.
	public function raw($strPath=null,$setRender=false)
	{
		if ($this->error) return;
		
		if ($strPath)
		{
			$this->forcePath($strPath);
		}
		
		if (!\Core\App::isInstalled())
		{
			$base = str_replace('\\','/',ABSPATH.FRAMEWORK).'skeleton/controllers/';
			$file = $base."core".".controller.php";
		}
		else
		{
			$base = str_replace('\\','/',ABSPATH).'src/controllers/';
			$file = $base.($this->controller).".controller.php";
		}
		
		$cName = $this->getControllerName();
		
		if (!file_exists($file))
		{
			$this->error = true;
			throw new \Exception(sprintf(__("O Controlador \"<strong>%s</strong>\" não foi encontrado. Certifique-se de que o arquivo existe em \"<strong>%s</strong>\"!"), $cName,$file),404);
		}
		require_once $file;
		
		if (!class_exists('\\App\\Controller\\'.$cName, false))
		{
			$this->error = true;
			throw new \Exception(sprintf(__("O arquivo \"<strong>%s</strong>\" existe, mas não foi encontrada a classe \"<strong>%s</strong>\" correspondente!"), $file, $cName),404);
		}
		
		$class = '\\App\\Controller\\'.$cName;
		
		$c = new $class();
		
		if (!method_exists($c, $this->action))
		{
			$this->error = true;
			throw new \Exception(sprintf(__("O método \"<strong>%s</strong>\" não existe na classe \"<strong>%s</strong>\"!"), $this->action, $cName),404);
		}
		
		$action = $this->action;
		
		$c->beforeFilter();
		
		$saida = call_user_func_array([$c,$this->action], ($this->vars ? $this->vars : $this->args));
		
		//$c->render();
		//$this->rendered = true;
		$c->afterFilter();
		
		if ($setRender) $this->setRendered(true);
		
		return $saida;
	}
	
	public function ALL($strPath="", $function=null)
	{
		if ($this->rendered) return false;
		if (!$this->isPath($strPath)) return false;
		
		if ($strPath) $this->forcePath($strPath);
		
		if ($function)
		{
			$function($this->args);
		}
		else
		{
			$this->render();
		}
	}
	
	public function GET($strPath="", $function=null)
	{
		if (!$this->is('GET')) return false;
		
		$this->ALL($strPath, $function);
	}
	
	public function POST($strPath="",$function=null)
	{
		if (!$this->is('POST')) return false;
		
		$this->ALL($strPath, $function);
	}
	
	public function PUT($strPath="",$function=null)
	{
		if (!$this->is('PUT')) return false;
		
		$this->ALL($strPath, $function);
	}
	
	public function DELETE($strPath="",$function=null)
	{
		if (!$this->is('POST')) return false;
		
		$this->ALL($strPath, $function);
	}
	
	public function METHOD($arrMethods,$strPath="", $function=null)
	{
		if ($this->rendered) return false;
		
		foreach($arrMethods as $method)
		{
			$m = strtoupper($method);
			
			if ($this->is($m))
			{
				if (method_exists($this,$m)) return $this->$m($strPath,$function);
				else return ($this->ALL($strPath,$function));
			}
		}
		return false;
	}
	
	public function getRequests()
	{
		$data = $_REQUEST;
		unset($data['path']);
		
		$this->data = $data;
	}
	
	# Pega uma variável definida na rota
	public function getVar($varname)
	{
		return @$this->vars[$varname];
	}
	
	# Retorna o método HTTP utilizado na requisição
	public function getMethod()
	{
		return $this->method;
	}
	
	# Pega um path em string e transforma-o em array ordenado
	public function processaPath($strPath)
	{
		if (strpos($strPath, '/') !== 0)
		{
			$strPath = "/./".$strPath;
			//pr(debug_backtrace());
		}
		$strPath = trim($strPath,'/');
		$path = explode('/',$strPath);
		
		//pr($path);
		
		$newpath = [
			'controller'	=> 'main',
			'action'		=> 'index',
			'args'			=> [],
		];
		
		if (sizeof($path) > 0)
			$newpath['controller'] = array_shift($path);
		
		if ($newpath['controller'] == '.')
			$newpath['controller'] = $this->controller;
		
		if (sizeof($path) > 0)
			$newpath['action'] = array_shift($path);
		
		if (sizeof($path) > 0)
			$newpath['args'] = $path;
		
		$newpath['controllerAlias'] = $this->getControllerAlias($newpath['controller']);
		
		return $newpath;
	}
	
	# Descobre se um $path em String corresponde ao caminho atual da página
	private function isPath($strPath)
	{
		$path = $this->processaPath($strPath);
		
		$controller = $path['controller'];
		$action = $path['action'];
		$args = $path['args'];
		$vars = [];
		
		if ($strPath == "") return true;
		
		if ($controller == ':controller')
		{
			$vars[':controller'] = $this->controller;
		}
		else if ($controller != $this->controller)
		{
			return false;
		}
		
		if (!$action and $args)
		{
			return false;
		}
		
		if ($action)
		{
			if ($action == ':action')
			{
				$vars[':action'] = $this->action;
			}
			else if ($action != $this->action)
			{
				return false;
			}
		}
		
		if ($args)
		{
			foreach($args as $i=>$arg)
			{
				if (strpos($arg,':') === 0)
				{
					$varName = ltrim($arg,':');
					$vars[$varName] = $this->args[$i];
				}
			}
		}
		
		$this->vars = $vars;
		
		return true;
	}
	
	# Pega o nome formatado de um Controller
	public function getControllerName($name=null)
	{
		$c = ($name) ? $name : $this->controller;
		$cName = $c.'.controller';
		$cName = CamelCase($cName);
		
		return $cName;
	}
	
	# Pega o alias de um Controller
	public function getControllerAlias($name=null)
	{
		$cName = $this->getControllerName($name);
		$alias = str_replace('Controller','',$cName);
		
		return $alias;
	}
	
	# Pega o diretório raiz da Aplicação
	public static function getRoot()
	{
		return str_replace('\\','/',ABSPATH);
	}
}