<?php
namespace App\View;

class AppView
{
	protected $path;
	protected $Controller;
	
	protected $Router;
	protected $App;
	
	private $vars = [];
	
	public $rendered = false;
	
	function __construct($Controller)
	{
		$this->Router = \Core\Router::getInstance();
		$this->App = \Core\App::getInstance();
		//$this->Router = new \Core\Router();
		
		$this->Controller = $Controller;
		
		$this->path = (\Core\App::isInstalled()) ? (str_replace('\\','/',ABSPATH)."src/views/") : (str_replace('\\','/',ABSPATH.FRAMEWORK)."skeleton/views/");
	}
	
	public function render($view=null)
	{
		if ($this->rendered) return;
		
		foreach($this->vars as $k=>$v)
		{
			$$k = $v;
		}
		
		if ($view)
		{
			$newPath = $this->Router->processaPath($view);
			$c = $newPath['controllerAlias'];
			$a = $newPath['action'];
		}
		else
		{
			$c = $this->Controller->alias;
			$a = $this->Controller->action;
		}
		$file = ($this->path)."{$c}/{$a}.php";
		
		if (!file_exists($file))
		{
			$this->Router->error = true;
			$this->rendered = true;
			throw new \Exception(sprintf(__("O arquivo de Visualização não foi encontrado em \"<strong>%s</strong>\"!"), $file),404);
		}
		
		ob_start();
		include $file;
		$content = ob_get_clean();
		
		if ($cLayout = $this->Controller->layout)
		{
			$layoutFile = ($this->path)."layouts/{$cLayout}.php";
			if (!file_exists($layoutFile))
			{
				$this->Router->error = true;
				$this->rendered = true;
				throw new \Exception(sprintf(__("O arquivo de Layout não foi encontrado em \"<strong>%s</strong>\"!"), $layoutFile),500);
			}
			
			$layout = [
				"content"	=> $content,
				"base"		=> \CORE\Router::getUrl('/',true),
			];
			$layout = (object)$layout;
			
			include $layoutFile;
		}
		else
		{
			echo $content;
		}
		
		$this->rendered = true;
	}
	
	public function element($elName,$vars=null)
	{
		$base = ($this->path)."elements/";
		$file = $base.$elName.'.php';
		
		$this->set('vars',$vars);
		
		if (!file_exists($file))
		{
			$this->Router->error = true;
			$debug = debug_backtrace();
			throw new \Exception(sprintf(__("O elemento \"<strong>%s</strong>\" não foi encontrado! \nArquivo: %s\nLinha: %s"), $file,$debug[0]['file'],$debug[0]['line']),500);
		}
		
		ob_start();
		include $file;
		$saida = ob_get_clean();
		
		return $saida;
	}
	
	public function set($varName, $value=NULL)
	{
		if (is_array($varName))
		{
			foreach($varName as $k=>$v)
			{
				$this->vars[$k] = $v;
			}
		}
		else
		{
			$this->vars[$varName] = $value;
		}
	}
	
	public function getMessage()
	{
		@session_start();
		$Msg = @$_SESSION['Message'];
		//var_dump($Msg);exit;
		if (!$Msg) return false;
		unset($_SESSION['Message']);
		return $Msg;
	}
}