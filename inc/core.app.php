<?php
namespace Core;

class App
{
	public $Router;
	
	function __construct()
	{
		$this->init();
	}
	
	public static function getInstance()
	{
		static $instance = null;
		if (null === $instance)
		{
			$instance = new static();
		}
		
		return $instance;
	}
	
	public static function isInstalled()
	{
		//var_dump(\Core\Router::getRoot());
		$base = str_replace('\\','/',ABSPATH).'src';
		return is_dir($base);
	}
	
	private function init()
	{
		try
		{
			self::importModule('Core.Router');
			$this->Router = \Core\Router::getInstance();
			
			$this->importConfig('routes');
		}
		catch(\Exception $e)
		{
			handleError($e);
		}
	}
	
	# Pega um módulo obrigatório do sistema
	public static function importModule($module)
	{
		$base = str_replace('\\','/',ABSPATH.FRAMEWORK);
		$file = $base."inc/".strtolower($module).".php";
		
		if (!file_exists($file))
		{
			die(__("Erro fatal: Não foi possível encontrar o módulo {$module}"));
		}
		else
		{
			require_once $file;
		}
	}
	
	# Pega um arquivo de configuração
	public function importConfig($type)
	{
		$fileBase = './src/config/';
		$errorMsg = '';
		$e = null;
		
		if (!self::isInstalled())
		{
			$base = str_replace('\\','/',ABSPATH.FRAMEWORK);
			$fileBase = $base.'skeleton/config/';
		}
		
		switch($type)
		{
			case 'routes':
				$filename = 'routes.php';
				$e = new NoRoutesException();
			break;
			default:
				$e = new InvalidConfigException();
				handleError($e);
				//$this->handleError(501,__("Invalid configuration!"),['arquivo'=>$type]);
			break;
		}
		
		$file = $fileBase.$filename;
		if (!file_exists($file))
		{
			//$this->handleError(500,($errorMsg ? $errorMsg : sprintf(__("Configuration file not found: %s"),$file)));
			handleError($e);
		}
		
		require_once($file);
	}
}

class InvalidConfigException extends \Exception
{
	function __construct()
	{
		$this->code = 501;
		$this->message = __("Invalid configuration!");
	}
}
class NoRoutesException extends \Exception
{
	function __construct()
	{
		$fileBase = './src/config/';
		
		$this->code = 500;
		$this->message = __("Não foi possível encontrar a configuração de Rotas.").' '.sprintf(__("Por favor, duplique o arquivo %s e o renomeie para %s"),$fileBase.'routes.php.default',$fileBase.'routes.php');
	}
}