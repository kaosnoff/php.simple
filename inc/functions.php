<?php

if (!function_exists('pr'))
{
	function pr($var)
	{
		echo "<pre>";
		print_r($var);
		echo "</pre>";
	}
}

if (!function_exists('__'))
{
	function __($var)
	{
		return $var;
	}
}

if (!function_exists('handleError'))
{
	function handleError($e)
	{
		$title = '';
		$msg = '';
		
		switch($e->getCode())
		{
			case 404:
				header("HTTP/1.0 404 Not Found");
				$title = __('Página não encontrada!');
				$msg = sprintf("Desculpe, mas não existe nada no endereço '<strong>%s</strong>'.",$_SERVER['REQUEST_URI']);
			break;
			case 500:
				header("HTTP/1.0 500");
				$title = __('Erro Interno!');
				$msg = sprintf("Desculpe, mas houve uma falha ao processar a página '<strong>%s</strong>'.",$_SERVER['REQUEST_URI']);
			break;
			case 501:
				header("HTTP/1.0 501");
				$title = __('Erro Interno!');
				$msg = sprintf("Desculpe, mas houve uma falha ao processar a página '<strong>%s</strong>'.",$_SERVER['REQUEST_URI']);
			break;
			default:
				header("HTTP/1.0 404 Not Found");
				$title = __('Página não encontrada!');
				$msg = sprintf("Desculpe, mas não existe nada no endereço '<strong>%s</strong>'.",$_SERVER['REQUEST_URI']);
			break;
		}
		
		?>
		<h1><?php echo $title; ?></h1>
		<p class="error"><?php echo $msg; ?></p>
		<pre><strong>Erro (<?php echo $e->getCode(); ?>): </strong><?php echo $e->getMessage(); ?></pre>
		<?php
		die();
		//var_dump($e->getMessage());
		//var_dump($e->getCode());
		//pr($e);
		//pr($msg);
	}
}

if (!function_exists('CamelCase'))
{
	function CamelCase($str)
	{
		$saida = preg_replace('/[\.\-\_]/',' ',$str);
		$saida = ucwords(strtolower($saida));
		$saida = str_replace(' ','',$saida);
		
		return $saida;
	}
}

if (!function_exists('DeCamelCase'))
{
	function DeCamelCase($str)
	{
		preg_match_all('/[A-Z]/', $str, $matches, PREG_OFFSET_CAPTURE);
		$saida = strtolower($str);
		
		$matches = @$matches[0];
		array_shift($matches);
		
		$i = 0;
		while ($matches)
		{
			$m = array_shift($matches);
			$pos = $m[1] + $i;
			$i++;
			
			$saida = substr_replace($saida,'_',$pos,0);
		}
		
		return $saida;
	}
}

if (!function_exists('getUrlVar'))
{
	function getUrlVar($url, $varName)
	{
		$query = parse_url($url, PHP_URL_QUERY);
		parse_str($query, $parts);
		
		return @$parts[$varName];
	}
}

if (!function_exists('getArrField'))
{
	function getArrField($array, $field)
	{
		if (!is_array($array)) return null;
		
		$saida = [];
		
		foreach($array as $k=>$v)
		{
			$saida[] = @$v[$field];
		}
		
		return $saida;
	}
}
