<?php
namespace App\Controller;

\Core\App::importModule('App.View');
\Core\App::importModule('App.Model');

class AppController
{
	public $alias;
	public $controller;
	public $action;
	protected $Router;
	protected $View;
	
	public $layout = null;
	
	function __construct()
	{
		$this->Router = \Core\Router::getInstance();
		//$this->Router = new \Core\Router();
		
		$cName = $this->Router->getControllerName();
		//$cName = str_replace(__NAMESPACE__,'',__CLASS__);
		//$cName = ltrim($cName,'\\');
		//var_dump($cName);
		$this->alias = str_replace('Controller','',$cName);
		$this->controller = $this->Router->controller;
		
		$this->action = $this->Router->action;
		
		//$this->Model = new \App\Model\AppModel();
		
		$this->View = new \App\View\AppView($this);
		
		$this->data = $this->Router->data;
	}
	
	public function beforeFilter()
	{
	}
	public function afterFilter()
	{
	}
	
	protected function set($varName,$value=null)
	{
		$this->View->set($varName,$value);
	}
	
	protected function json($vars)
	{
		echo json_encode($vars);
		$this->View->rendered = true;
	}
	
	public function render($view=null)
	{
		$this->View->render($view);
	}
	
	protected function redirect($strPath)
	{
		$this->Router->redirect($strPath);
		exit;
	}
	
	protected function loadModel($modelName)
	{
		$path = str_replace('\\','/',ABSPATH)."src/models/";
		
		$modelFileName = DeCamelCase($modelName);
		
		$modelFile = $path.$modelFileName.".model.php";
		
		if (!file_exists($modelFile))
		{
			$this->Router->error = true;
			
			throw new \Exception(sprintf(__("O arquivo de Modelo para \"<strong>%s</strong>\" não foi encontrado em \"<strong>%s</strong>\"!"), $modelName, $modelFile),500);
		}
		
		include $modelFile;
		
		$className = "\\App\\Model\\".$modelName;
		$model = new $className();
		
		$model->Router = $this->Router;
		
		//$model = \App\Model\AppModel::load($modelName);
		return $model;
	}
	
	public function getMethod()
	{
		return $this->Router->getMethod();
	}
	
	protected function setMessage($message,$tipo='info')
	{
		$_SESSION['Message'] = [
			'msg'	=> $message,
			'tipo'	=> $tipo,
		];
		session_commit();
		session_write_close();
		//var_dump($_SESSION);
		//exit;
	}
	protected function setError($message)
	{
		$this->setMessage($message,'error');
	}
	protected function setSuccess($message)
	{
		$this->setMessage($message,'success');
	}
}
