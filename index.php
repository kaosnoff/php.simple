<?php
session_start();

$framework = 'simplephp/';

require_once "{$framework}inc/functions.php";

/** caminho absoluto para a pasta do sistema **/
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '\\');
	
/** caminho no server para o sistema **/
if ( !defined('BASEURL') )
{
	$path1 = $_SERVER['CONTEXT_DOCUMENT_ROOT'];
	$path2 = __DIR__;
	$path1 = str_replace('\\','/',$path1);
	$path2 = str_replace('\\','/',$path2);

	$path = str_replace($path1,'',$path2).'/';
	define('BASEURL', $path);
}	

require_once "{$framework}inc/core.app.php";

$App = \Core\App::getInstance();
