<?php
# Rotas customizadas
$Router = \Core\Router::getInstance();

# Define a página a ser renderizada na raiz do site
$Router->GET('/',function() use($Router)
{
	$Router->render('/core/index');
});

# Pega todas as páginas que não se enquadrarem nos filtros acima
$Router->ALL();