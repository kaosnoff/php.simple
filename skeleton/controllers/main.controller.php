<?php
namespace App\Controller;

class MainController extends AppController
{
	public $layout = 'default';
	
	public function index()
	{
		$this->set('title','SimplePHP framework');
	}
}