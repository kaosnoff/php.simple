<?php
namespace App\Controller;

class CoreController extends AppController
{
	public $layout = 'default';
	
	public function index()
	{
		//$this->layout = 'default';
		//$this->set('title','Consegui');
		
		if (!\Core\App::isInstalled())
		{
			$this->redirect('/core/install');
		}
	}
	
	public function install($confirm=null)
	{
		if ($confirm == 'yes')
		{
			$base = str_replace('\\','/',ABSPATH).'src/';
			$skel = str_replace('\\','/',ABSPATH.FRAMEWORK).'skeleton/';
			
			#root
			@mkdir($base,0777,true);
			$file = '.htaccess';
			copy($skel.$file,$base.$file);
			
			#config
			$dir = 'config/';
			@mkdir($base.$dir,0777,true);
			
			$file = 'config.database.php.default';
			copy($skel.$dir.$file,$base.$dir.$file);
			$file = 'routes.php.default';
			copy($skel.$dir.$file,$base.$dir.$file);
			rename($base.$dir.$file,$base.$dir.'routes.php');
			
			#controllers
			$dir = 'controllers/';
			@mkdir($base.$dir,0777,true);
			
			$file = 'main.controller.php';
			copy($skel.$dir.$file,$base.$dir.$file);
			$file = 'empty';
			copy($skel.$dir.$file,$base.$dir.$file);
			
			#models
			$dir = 'models/';
			@mkdir($base.$dir,0777,true);
			
			$file = 'exemplo.model.php';
			copy($skel.$dir.$file,$base.$dir.$file);
			$file = 'empty';
			copy($skel.$dir.$file,$base.$dir.$file);
			
			#public
			$dir = 'public/';
			@mkdir($base.$dir,0777,true);
			
			$file = '.htaccess';
			copy($skel.$dir.$file,$base.$dir.$file);
			$file = 'empty';
			copy($skel.$dir.$file,$base.$dir.$file);
			
			#views
			$dir = 'views/';
			@mkdir($base.$dir,0777,true);
			
			$file = 'empty';
			copy($skel.$dir.$file,$base.$dir.$file);
			
			#views/Main
			$dir = 'views/Main/';
			@mkdir($base.$dir,0777,true);
			
			$file = 'index.php';
			copy($skel.$dir.$file,$base.$dir.$file);
			
			#views/elements
			$dir = 'views/elements/';
			@mkdir($base.$dir,0777,true);
			
			$file = 'empty';
			copy($skel.$dir.$file,$base.$dir.$file);
			
			#views/layouts
			$dir = 'views/layouts/';
			@mkdir($base.$dir,0777,true);
			
			$file = 'empty';
			copy($skel.$dir.$file,$base.$dir.$file);
			$file = 'default.php';
			copy($skel.$dir.$file,$base.$dir.$file);
			
			$this->redirect('/');
		}
	}
	
	
	/*
	public function teste($id=null)
	{
		$saida = [];
		$this->json($saida);
	}
	// */
}