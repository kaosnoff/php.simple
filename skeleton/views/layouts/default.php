<!DOCTYPE html>
<html lang="pt_BR">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Framework CAUT</title>
	
	<base href="<?php echo $layout->base; ?>">
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="./public/js/jquery-1.11.2.min.js"><\/script>')</script>
	
	<link rel="stylesheet" href="./public/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">
	<link rel="stylesheet" href="./public/css/style.css">
	
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="./" class="navbar-brand">Início</a>
			</div>
		</div>
	</nav>
	
	<main class="container">
		<?php
		echo $layout->content;
		?>
	</main>
	
	<hr>
	<footer class="container">
		<p>&copy;2017 Superior Tribunal de Justiça</p>
	</footer>
	
    <script src="./public/js/bootstrap.min.js"></script>
	<script src="./public/js/main.js"></script>
</body>
</html>