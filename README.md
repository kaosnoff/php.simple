#SimplePHP Framework 1.0

Este projeto é uma criação de Vitor Dutra (kaosnoff.com.br) com o objetivo de criar um framework simples e rápido para páginas simples e APIs REST.

##Instalação

Não adicione este repositório diretamente. Ao invés disso, crie um novo projeto com `git init` e adicione o SimplePHP através do comando `git submodule add https://bitbucket.org/kaosnoff/php.simple.git simplephp`.

Após criar o projeto e adicionar o submódulo, acesse através do navegador a URL do seu projeto local (é necessário um servidor Apache) e clique no botão de instalação. O programa de instalação irá criar uma estrutura padrão na pasta `.src`.

Caso já possua um projeto ativo, após cloná-lo em uma nova máquina, é necessário rodar o comando: 

``git submodule update --init``

e em seguida:

``git submodule update --remote simplephp``

## Detalhes gerais

O SimplePHP utiliza a arquitetura MVC - Model, View, Controller - para a divisão dos componentes.

## Atualizando o SimplePHP

Para atualizar o framework, rode o comando:

`git submodule update --remote simplephp`

ou vá até a pasta do SimplePHP e rode o comando:

`git pull`